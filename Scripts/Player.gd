extends CharacterBody2D

signal shoot

var speed = 200.0
var can_shoot: bool
var can_move: bool
var add = 0

@onready var debug = $debug
@onready var progressBar = $ProgressBar
@onready var _typingMechanic = $TypingMechanic
@onready var ghost_timer = $ShotTimer

@export var ghost_node : PackedScene

var health = 100:
	set(value):
		health = value
		progressBar.value = value

func _ready():
	can_shoot = true
	can_move = true
	
func _physics_process(delta):
	#if Input.is_action_just_pressed("ui_accept"):
		#add += 1
		#print(add)
		#get_parent().active_boss(add)
	if health <= 0:
		get_tree().reload_current_scene()
		
	if  Input.is_action_just_pressed("dash"):
		dash()
		
	var direction = Input.get_vector("move_left", "move_right", "move_up","move_down")
	velocity = direction * speed
	if can_move == true:
		get_input()
		move_and_slide()
		
func get_input():
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
		$TypingMechanic.visible = false
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_RIGHT):
		$TypingMechanic.visible = true
		
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) and can_shoot:
		print(Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT))
		print(can_shoot)
		var dir = get_global_mouse_position() - position
		#shoot.emit(position, dir)
		can_shoot = false
		$ShotTimer.start()

func _on_shot_timer_timeout():
	can_shoot = true
	add_ghost()
	
func set_status(bullet_type):
	match bullet_type:
		0:
			fire()
		1:
			poison()
		2:
			slow()
		3:
			stun()

func add_ghost():
	var ghost = ghost_node.instantiate()
	ghost.set_property(position, $Sprite2D.scale)
	get_tree().current_scene.add_child(ghost)

func dash():
	ghost_timer.start()
	var tween = get_tree().create_tween()
	tween.tween_property(self, "position", position + velocity * 1.5, 0.45)
	await tween.finished
	ghost_timer.stop()

func fire():
	debug.text = "fire"
	health -= 10
	
func poison():
	debug.text = "poison"
	for i in range(5):
		health -= 2
		await get_tree().create_timer(1).timeout

func slow():
	debug.text = "slow"
	speed = 50

func stun():
	debug.text = "stun"
	speed = 0
	await get_tree().create_timer(2.5).timeout
	speed = 200

func set_bool(isMove: bool):
	can_move = isMove
