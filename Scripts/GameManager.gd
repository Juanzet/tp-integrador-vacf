extends Node

@onready var bos1 = $BossManager
@onready var bos2 = $BossManager2
@onready var bos3 = $BossManager3

@onready var levels = $Levels

#get.tree().change_scene_to_packed(nombre de la escena)

func _ready():
	bos1.visible = false
	bos2.visible = false
	bos3.visible = false

func _process(delta):
	pass 
	
func active_boss(current_boss):
	match current_boss:
		1:
			$Levels.level_selection(1)
			bos1.visible = true
			bos2.visible = false
			bos3.visible = false
		2: 
			$Levels.level_selection(2)
			bos1.visible = false
			bos2.visible = true
			bos3.visible = false
			#get_tree().change_scene_to_packed(enojoScene)
		3:
			$Levels.level_selection(3)
			bos1.visible = false
			bos2.visible = false
			bos3.visible = true
			#get_tree().change_scene_to_packed(desesperanzaScene)
			
func show_boss(boss:bool):
	bos1.visible = boss
	
