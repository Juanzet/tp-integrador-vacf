extends State
@onready var collision  = %CollisionShape2D
@onready var singleton = get_parent()

var player_entered : bool = false:
	set(value):
		player_entered = value
		collision.set_deferred("disabled", value)

func _on_player_entered(body):
	if body.name == "Player":
		player_entered = true

func transition():
	if player_entered:
		get_parent().change_state("5Leaf")

func set_bool(isMove: bool):
	isMove = player_entered
