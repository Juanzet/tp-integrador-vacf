extends State

func enter():
	super.enter()
	owner.bullet_type = 3
	owner.alpha = 3
	
func transition():
	if can_transition:
		get_parent().change_state("5Leaf")
