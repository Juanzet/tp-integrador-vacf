extends Area2D

var speed: int = 500
var dmg = 10
var direction: Vector2

func _process(delta):
	position += speed * direction * delta

func _on_timer_timeout():
	queue_free()

func _on_body_entered(body):
	if body.name == "Enemy" and body.alive:
		body.take_damage(dmg)
