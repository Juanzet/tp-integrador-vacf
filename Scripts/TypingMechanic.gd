extends Node2D

@export var word_to_type = ["exorcist", "demond", "purge", "D MAJOREM DEI GLORIAM",
 "CRUX AVE, SPES UNICA", "DEO GRATIAS", "GLORIA PATRI ET FILIO ET SPIRITUI SANCTO"]

var current_word = ""
var typed_word = ""

@onready var label = $Label
@onready var input_field = $LineEdit
@onready var player = get_parent()

func _ready():
	start_new_word()

func _process(delta):
	handle_typing()

func start_new_word():
	current_word = word_to_type[randi() % word_to_type.size()].to_lower()
	typed_word = ""
	label.text = current_word
	print("type this word: %s" % current_word)

func handle_typing():
	typed_word = input_field.text
	if typed_word == current_word:
		print("Word completed!")
		start_new_word()
		deal_damage(current_word.length())
		input_field.text = ""
	elif not current_word.begins_with(typed_word):
		print("Mistyped! Penalty!")
		typed_word = ""
		input_field.text = ""
		
func deal_damage(amount):	
	for child in get_tree().root.get_node("Main").get_children():
		if child.is_in_group("Enemy"):
			print("le hiciste " + str(amount) + " de dmg")
			child.take_damage(amount)	
	
	#codigo para mover o no a l personaje "get_parent().set_bool(false)"

func _on_line_edit_focus_entered():
	player.set_bool(false)

func _on_line_edit_focus_exited():
	player.set_bool(true)
