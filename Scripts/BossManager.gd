extends CharacterBody2D

var theta: float = 0.0
@export_range(0,2*PI) var alpha: float = 0.0
@export var bullet_node: PackedScene
@export var icon : CompressedTexture2D
@export var bossName : String
var bullet_type : int = 0
@export var max_health : int = 20
var current_health = 0
var currentBoss = 0
 
func _ready():
	$Sprite2D.texture = icon
	current_health = max_health
	

func _process(delta):
	if self.visible == false:
		%CollisionShape2D.disabled = true
	else:
		%CollisionShape2D.disabled = false
	
func take_damage(dmg):
	current_health -= dmg
	if current_health <= 0:
		#queue_free() este es para eliminar el jefe 
		QuestManager.next_quest()
		hide() #este es para esconder el jefe 
		currentBoss += 1
		get_parent().active_boss(currentBoss)
		#$FiniteStateMachine.queue_free()
		#get_parent().level_selection(currentBoss)
		print(currentBoss)
	print(bossName)
	match bossName:
		"Tristesa":
			QuestManager.next_quest()
		"Enojo": 
			QuestManager.next_quest()
		"Desesperanza":
			QuestManager.next_quest()
		
func get_vector(angle):
	theta = angle + alpha
	return Vector2(cos(theta), sin(theta))
	
func shoot(angle):
	var bullet = bullet_node.instantiate()
	
	bullet.position = global_position
	bullet.direction = get_vector(angle)
	bullet.set_property(bullet_type)
	
	get_tree().current_scene.call_deferred("add_child", bullet)
	
#golden ratio, from numberphile, why prime makes spiral, from 3blue1brom PARA MAS PATRONES
func _on_speed_timeout():
	shoot(theta)

