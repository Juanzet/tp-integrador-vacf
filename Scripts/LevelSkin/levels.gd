extends Node2D

#@export var levels : Array[TileMap]
@onready var level_one = $OutLevel
@onready var level_two = $IndoorBoss01
@onready var level_three = $IndoorBoss02

func level_selection(select : int):
	if select == 1:
		level_one.visible = true
		level_two.visible = false
		level_three.visible = false
	elif select == 2:
		level_one.visible = false
		level_two.visible = true
		level_three.visible = false
	elif select == 3:
		level_one.visible = false
		level_two.visible = false
		level_three.visible = true
