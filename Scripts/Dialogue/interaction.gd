extends Area2D

@onready var gm = get_parent().get_parent().get_parent()
var can_active_boss = true
var can_interaction = false:
	set(value):
		can_interaction = value
		%Label.visible = value

func _on_body_entered(body):
	can_interaction = true

func _on_body_exited(body):
	can_interaction = false

func interaction():
	owner.interact()
	print(owner.name + " interacted.")

func _input(event):
	if event.is_action_pressed("interact") and can_interaction:
		if can_active_boss:
			gm.show_boss(true)
			can_active_boss = false
		interaction()
